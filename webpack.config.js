const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
  entry: {
    index: './client/index.tsx',
  },
  output: {
    filename: 'bundle.[name].js?[contenthash]',
    chunkFilename: 'bundle.[name].js?[contenthash]',
    path: __dirname + '/.client-dist',
  },
  devtool: 'source-map',
  resolve: {
      extensions: ['.ts', '.tsx', '.js', '.json'],
  },
  module: {
    rules: [
      { test: /\.tsx?$/, loader: 'awesome-typescript-loader', exclude: /node_modules/  },
      { test: /\.css?$/, loader: ['style-loader', 'css-loader'], exclude: /node_modules/ },
      { test: /\.(jpe?g|gif|png|svg|woff|ttf)$/, loader: "file-loader", exclude: /node_modules/ },
      { test: /\.ftl$/, loader: 'raw-loader', exclude: /node_modules/ },
      { enforce: 'pre', test: /\.js$/, loader: 'source-map-loader', exclude: /node_modules/ },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './client/index.html',
      inject: 'body',
    }),
  ],
  optimization: {
    minimizer: [new TerserPlugin()],
    splitChunks: {
      chunks: 'all'
    }
  },
  node: {
    fs: 'empty',
  },
};
