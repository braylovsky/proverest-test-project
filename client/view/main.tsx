import * as React from 'react';
import { useSelector } from 'react-redux';
import { IReduxState } from '../store';
import { ContactsTable, EditWindow } from '../components';

export const Main = () => {
  const { contacts, mode } = useSelector((store: IReduxState) => store.contacts);

  React.useEffect(() => {
    localStorage.setItem('contacts', JSON.stringify(contacts));
  }, [contacts]);

  return <>
    <div>
      <ContactsTable contacts={contacts} />
      {mode !== 'view' ? <EditWindow windowMode={mode === 'add' ? 'add' : 'edit'} /> : <></>}
    </div>
  </>;
};
