import { combineReducers } from 'redux';
import * as Contacts from './contacts';

export interface IState {
  contacts: Contacts.IState;
}

export const initialState: IState = {
  contacts: Contacts.initialState,
};

export const combinedReducers = combineReducers<IState>({
  contacts: Contacts.reducer,
});
