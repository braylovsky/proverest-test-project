import * as Action from '../actions/contacts';
import { IContact, IContacts, Mode } from '../types';

export interface IState {
  mode: Mode;
  currentContact?: IContact;
  contacts: IContacts;
}

export const initialState: IState = {
  mode: 'view',
  contacts: (localStorage.getItem('contacts') ? JSON.parse(localStorage.getItem('contacts')) : []),
};

export const reducer = (state: IState = initialState, action: Action.IAction): IState => {
  switch (action.type) {

    case Action.Type.addContact: {
      const id = state.contacts.map((val) => val.id).reduce((acc, cur) => acc < cur ? cur : acc, 0) + 1;

      return {
        ...state,
        contacts: [...state.contacts, Object.assign(action.payload.contact, { id })],
      };
    }

    case Action.Type.editContact: {
      const position = state.contacts.findIndex((val) => val.id === action.payload.contact.id);

      return {
        ...state,
        contacts: [...state.contacts.slice(0, position), action.payload.contact, ...state.contacts.slice(position + 1)],
      };
    }

    case Action.Type.deleteContact: {
      return {
        ...state,
        contacts: state.contacts.filter((val) => val.id !== action.payload.id),
      };
    }

    case Action.Type.changeMode: {
      return {
        ...state,
        currentContact: action.payload.currentContact,
        mode: action.payload.mode,
      };
    }

    default: return state;
  }
};
