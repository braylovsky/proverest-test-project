import * as React from 'react';
import { Router, Route } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { Main } from './view/main';

const history = createBrowserHistory();

export default () => (
  <Router history={history}>
    <>
      <Route exact path={`/`} component={Main} />
    </>
  </Router>
);

export { history };
