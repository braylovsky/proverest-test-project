import styled from 'styled-components';

export const Table = styled.table`
  border-collapse: collapse;
  border: 1px solid black;
`;

export const Cell = styled.td`
  border: 1px solid black;
  padding: 5px;
`;

export const Panel = styled.div`
  margin: 20px;
`;

export const Button = styled.div`
  border: 1px solid black;
  margin-bottom: 10px;
  padding: 5px;
  display: inline-block;
`;

export const ButtonIcon = styled.img`
  height: 25px;
  width: 25px;
`;

