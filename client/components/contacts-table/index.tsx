import React from 'react';
import { IContacts } from '../../types';
import { Panel, Table, Cell, Button, ButtonIcon } from './style';
import { Store, IReduxState } from '../../store';
import Actions from '../../actions';
import { useSelector } from 'react-redux';

export const ContactsTable = ({ contacts }: IProps) => {
  const mode = useSelector((store: IReduxState) => store.contacts.mode);

  return <>
    <Panel>
      <h3>Contacts Table</h3>
      <Button
        onClick={() => {
          if (mode === 'view') {
            Store.dispatch(Actions.Contacts.changeMode('add'));
          }
        }}
      >add</Button>
      <Table>
        <thead>
          <tr>
            <Cell width='200px'>Name</Cell>
            <Cell width='150px'>Email</Cell>
            <Cell width='150px'>Phone</Cell>
            <Cell width='50px'>Action</Cell>
          </tr>
        </thead>
        <tbody>
          {contacts.map(({ id, name, email, phone }, i) =>
            <tr key={i}>
              <Cell>{name}</Cell>
              <Cell>{email}</Cell>
              <Cell>{phone}</Cell>
              <Cell>
                <ButtonIcon
                  src={require('../../icons/edit.svg')}
                  onClick={() => {
                    if (mode === 'view') {
                      Store.dispatch(Actions.Contacts.changeMode('edit', { id, name, email, phone }));
                    }
                  }}
                />
                <ButtonIcon
                  src={require('../../icons/delete.svg')}
                  onClick={() => {
                    if (mode === 'view') {
                      Store.dispatch(Actions.Contacts.deleteContact(id));
                    }
                  }}
                />
              </Cell>
            </tr>,
          )}
        </tbody>
      </Table>
    </Panel>
  </>;
};


interface IProps {
  contacts: IContacts;
}
