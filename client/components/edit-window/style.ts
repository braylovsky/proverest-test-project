import styled from 'styled-components';

export const Window = styled.div`
  border: 1px solid black;
  display: inline-block;
  position: absolute;
  left: 50%;
  top: 50%;
  height: 110px;
  margin-top: -55px;
  margin-left: -160px;
  background-color: white;
`;

export const TabsBlock = styled.div`
  height: 30px;
`;

export const Tab = styled.div<{ active: boolean }>`
  font-weight: ${(p) => p.active ? 'bold' : 'normal'}
  border: 1px solid black;
  padding: 5px;
  float: left;
  height: 20px;
  width: 90px;
`;

export const ButtonBlock = styled.div`
  padding-right: 10px;
`;

export const Button = styled.div`
  border: 1px solid black;
  padding: 5px;
  margin-left: 5px;
  display: inline-block;
  float: right;
`;

export const InputBlock = styled.div`
  padding-left: 10px;
  padding-top: 10px;
  padding-bottom: 10px;
`;

export const Input = styled.input`
  width: 280px;
`;
