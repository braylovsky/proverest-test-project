import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import Actions from '../../actions';
import { IReduxState } from '../../store';
import { Window, TabsBlock, Tab, ButtonBlock, Button, InputBlock, Input } from './style';
import { WindowMode } from '../../../types';
import Store from '../../store';

type TabName = 'name' | 'email' | 'phone';

export const EditWindow = ({ windowMode }: IProps) => {

  const currentContact = useSelector((store: IReduxState) => store.contacts.currentContact);

  const [contact, setContact] = useState(windowMode === 'edit' ? currentContact : { name: '', phone: '', email: '' });
  const [currentTab, setCurrentTab] = useState('name' as TabName);

  return <>
    <Window>
      <TabsBlock>
        <Tab active={currentTab === 'name'} onClick={() => setCurrentTab('name')}>Name</Tab>
        <Tab active={currentTab === 'email'} onClick={() => setCurrentTab('email')}>Email</Tab>
        <Tab active={currentTab === 'phone'} onClick={() => setCurrentTab('phone')}>Phone</Tab>
      </TabsBlock>
      <InputBlock>
        {currentTab === 'name' ? <Input
          defaultValue={windowMode === 'edit' ? contact.name : ''}
          onChange={(e) => setContact(Object.assign(contact, { name: e.target.value }))}
        /> : <> </>}
        {currentTab === 'email' ? <Input
          defaultValue={windowMode === 'edit' ? contact.email : ''}
          onChange={(e) => setContact(Object.assign(contact, { email: e.target.value }))}
        /> : <></>}
        {currentTab === 'phone' ? <Input
          defaultValue={windowMode === 'edit' ? contact.phone : ''}
          onChange={(e) => setContact(Object.assign(contact, { phone: e.target.value }))}
        /> : <></>}
      </InputBlock>
      <ButtonBlock>
        <Button onClick={() => Store.dispatch(Actions.Contacts.changeMode('view'))}>Cancel</Button>
        <Button onClick={() => {
          Store.dispatch(windowMode === 'add' ? Actions.Contacts.addContact(contact) : Actions.Contacts.editContact(contact));
          Store.dispatch(Actions.Contacts.changeMode('view'));
        }}>{windowMode === 'add' ? 'Add' : 'Save'}</Button>
      </ButtonBlock>
    </Window>
  </>;
};

export interface IProps {
  windowMode: WindowMode;
}
