import { action } from 'typesafe-actions';
import { IContact, Mode } from '../types';

export enum Type {
  addContact = '[contacts] add',
  editContact = '[contacts] edit',
  deleteContact = '[contacts] delete',
  changeMode = '[contacts] change mode',
}

export const addContact = (contact: IContact) => action(Type.addContact, { contact });
export const editContact = (contact: IContact) => action(Type.editContact, { contact });
export const deleteContact = (id: number) => action(Type.deleteContact, { id });
export const changeMode = (mode: Mode, currentContact?: IContact) => action(Type.changeMode, { mode, currentContact });

export type IAction =
  | ReturnType<typeof addContact>
  | ReturnType<typeof editContact>
  | ReturnType<typeof deleteContact>
  | ReturnType<typeof changeMode>
  ;
