import { createStore } from 'redux';
import { combinedReducers, IState } from './reducers';

export const Store = createStore(combinedReducers);

export default Store;

export {
  IState as IReduxState,
};
