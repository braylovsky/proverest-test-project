* Installation

1. Install stable nodeJS: https://nodejs.org/
2. Run "npm i" in the project directory

* Configuring

Configuration file path - ./config/config.json

* How to use

1. Run "npm start" in the project directory
2. Open browser, go to address localhost:3000
