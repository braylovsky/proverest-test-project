import Express from 'express';
import * as path from 'path';
import { IConfig } from '../types/config';

export const app = Express();

const config: IConfig = require('../../config/config.json');

app.disable('x-powered-by');
app.disable('etag');

app.use('/', Express.static(`${__dirname}/../../.client-dist`));
app.get(`*`, (req, res, next) => {
  res.sendFile(path.resolve(`${__dirname}/../../.client-dist/index.html`));
});

app.listen(config.http.port, config.http.ip, () => {
  console.log(`Started HTTP server at ${config.http.ip}:${config.http.port}`);
});
