export interface IConfig {
  http: {
    ip: string;
    port: number;
  };
}
