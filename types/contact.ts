export interface IContact {
  id?: number;
  name: string;
  email: string;
  phone: string;
}

export type IContacts = IContact[];

export type Mode = 'view' | 'add' | 'edit';

export type WindowMode = 'add' | 'edit';
